bulma-components v0.0.4
=======================

React components for [Bulma](http://bulma.io/)
----------------------------------------------

# Install

```npm
npm install bulma-components
```

# Use

```js
// import bulma css
import 'bulma/css/bulma.css'
// import DatePicker css, view flatpickr docs
import 'flatpickr/dist/themes/material_green.css'
// translate DatePicker
import {es} from 'flatpickr/dist/l10n/es.js'
// import react
import React from 'react'
import {render} from 'react-dom'

// import from components
import {Button, DatePicker} from 'bulma-components'

render(
  (
    <div>
      // font awesome is required for icons
      <Button icon='download' className='is-primary'>bulma-components</Button>
      <br />
      <DatePicker locale={es} min={new Date()} />
    </div>
  ),
document.getElementById('app'))
```

# Components

-  [x] Addon
-  [x] Button
-  [x] CheckList
-  [x] Columns
-  [x] Container
-  [x] DatePicker
-  [x] Footer
-  [x] Hero
-  [x] Icon
-  [x] Layout
-  [x] Level
-  [x] LevelItem
-  [x] LevelResume
-  [x] Message
-  [x] RadioGroup
-  [x] SelectField
-  [x] Tabs
-  [x] TextField
-  [x] Tile

## Callback Property

Use callback property to receive the new state of component

```js
const printState = (state) => {
  console.log(state)
}

<TextField callback={printState} />
<DatePicker callback={printState} />
```

Use In:

* CheckList
* DatePicker
* RadioGroup
* SelectField
* Tabs
* TextField

## Addon

Use addon for join elements

```js
<Addon>
  <SelectField />
  <TextField />
  <Button>...</Button>
</Addon>
```

## Button

* Props
  + type (html button type)
  + icon (icon name, fa-[icon])
  + iconRight (align to right)

```js
<Button icon='download' iconRight>bulma-components</Button>
```

## CheckList

* Props
  + type (javascript data type, default: String)
  + name (html input name)
  + values (array of values, default: [])
  + options (array of options, default: [])

```js
<CheckList name='check1' type={Number} values={[1, 3]} options={[
  {label: '$ 1', value: 1},
  {label: '$ 2', value: 2},
  {label: '$ 3', value: 3},
  {label: '$ 4', value: 4},
  {label: '$ 5', value: 5}
]} />

<CheckList name='check2' values={[]} options={[
  'a', 'b', 'c', 'd'
]} />
```

## Columns

* Props
  + multiline (boolean, default: false)
  + gapless (boolean, default: false)
  + device ('mobile', 'tablet', 'desktop', default: '')

```js
<Columns device='mobile' multiline gapless>...</Columns>
```

### Column

* Props
  + size (1/4, 1/3, 1/2, 2/3, 3/4, 1, 2, ..., 11, default: null)
  + offset (1/4, 1/3, 1/2, 2/3, 3/4, 1, 2, ..., 11, default: null)

```js
<Columns>
  <Column size='1/4' offset='1/4'>...</Column>
  <Column size='1/4'>...</Column>
</Columns>
```

## DatePicker

* Props
  + locale (translate object)
  + min (min date)
  + max (max date)

```js
<DatePicker locale={es} min={new Date()} max={new Date('2050-12-31')} />
```

## Icon

Use icon names, view [Font Awesome](http://fontawesome.io/icons/)

```js
<Icon>plus</Icon>
```

## Layout

### Container

```js
<Container className='is-fluid'>Content</Container>
```

### Footer

```js
<Footer>
  <Container>
    <div className='content has-text-centered'>
      Copyright ...(c) 2016 Copyright Holder All Rights Reserved.
    </div>
  </Container>
</Footer>
```

### Hero

* Props
  + size ('is-medium', 'is-large', 'is-fullheight', default: '')
  + header (React Component)
  + footer (React Component)

```js
<Hero
  size='is-fullheight'
  className='is-primary is-bold'
  header={<Component />}
  footer={<Component />}
>
  <Container>
    <h1 className='title'>
      title
    </h1>
    <h2 className='subtitle'>
      subtitle
    </h2>
  </Container>
</Hero>
```

## Level

Use level with LevelItem or LevelResume

### LevelResume

* Props
  + heading
  + title

```js
<Level>
  <LevelResume heading='example' title='1' />
  <LevelResume heading='example' title='2' />
  <LevelResume heading='example' title='3' />
  <LevelResume heading='example' title='4' />
</Level>
```

### LevelItem

* Props
  + type (use 'item', 'left', 'right', default: 'item')

```js
<Level>
  <LevelItem type='left'>LEFT CONTENT</LevelItem>
  <LevelItem type='right'>RIGHT CONTENT</LevelItem>
</Level>
```

## Message

* Props
  + header (text)

```js
<Message>Message 1, Body Text</Message>

<Message header='Message 2' >Body text</Message>
```

## RadioGroup

* Props
  + type (javascript data type, default: String)
  + name (html input name)
  + value (any data type values, default: null)
  + options (array of options, default: [])

```js
<RadioGroup
  options={['one', 'two', 'three']}
  value='two'
  name='radio1'
/>

<RadioGroup
  options={[
    {label: '$100', value: 100},
    {label: '$200', value: 200},
    {label: '$300', value: 300}
  ]}
  value={100}
  name='radio2'
  type={Number}
/>
```

## SelectField

* Props
  + value (any data type values, default: null)
  + options (array of options, default: [])

```js
<SelectField
  options={['one', 'two', 'three']}
  value='two'
/>

<SelectField
  options={[
    {label: '$100', value: 100},
    {label: '$200', value: 200},
    {label: '$300', value: 300}
  ]}
  value={100}
/>
```

## Tabs

* Props
  + elements (array, default: [])
  + active (element active)
  + position ('left', 'right', default: 'left')
  + container (boolean, default: false)

```js
<Tabs
  elements={[1, 2, 3, 4, 5]}
  active={5}
  className='is-boxed'
  position='right'
/>

<Tabs
  elements={[
    {
      label: 'Images',
      content: (<a>
        <Icon partner>image</Icon>
        <span>Pictures</span>
      </a>)
    },
    {
      label: 'Music',
      content: (<a>
        <Icon partner>music</Icon>
        <span>Music</span>
      </a>)
    },
    {
      label: 'Video',
      content: (<a>
        <Icon partner>film</Icon>
        <span>Video</span>
      </a>)
    }
  ]}
/>
```

## TextField

* Props
  + area (boolean, default: false)
  + type (html input type, default: text)
  + placeholder (text, default: '')
  + icon (icon name)
  + iconRight (boolean: default: false)
  + required (boolean: default: false)
  + success (text, default: 'OK')
  + error (text, default: 'Error!')
  + warning (text, default: 'Warning!')
  + pattern (javascript regexp, default: new RegExp()),
  + area (boolean: false),
  + control (text, default: '')

```js
<label className='label'>Example</label>
<TextField
  placeholder='Text Example'
  pattert={/[a-zñ]+/i}
  required
/>
<TextField
  placeholder='Custom Message'
  pattert={/[a-zñ]+/i}
  error='uuups! :('
  warning='is required'
  success=':)'
  iconRight
  required
/>
<TextField
  placeholder='Password'
  type='password'
  icon='lock'
  required
/>
<TextField
  placeholder='Textarea example'
  required
  area
/>
<TextField
  placeholder='Textarea example'
  control='is-loading'
  area
/>
```

## Tile

* Props
  + ancestor (boolean, default: false)
  + parent (boolean, default: false)
  + child (boolean, default: false)
  + vertical (boolean, default: false)
  + size (1/4, 1/3, 1/2, 2/3, 3/4, 1, 2, ..., 11 , default: '')

```js
<Tile ancestor>
  <Tile vertical size='8'>
    <Tile>
      <Tile parent vertical>
        <Tile child className='box'>...</Tile>
        <Tile child className='box'>...</Tile>
      </Tile>
      <Tile parent>
        <Tile child className='box'>...</Tile>
      </Tile>
    </Tile>
    <Tile parent>
      <Tile child className='box'>...</Tile>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child className='box'>...</Tile>
  </Tile>
</Tile>
```
