import React, {Component} from 'react'

export default class BulmaMessage extends Component {
  render () {
    return (
      <article className={`message ${this.props.className}`}>
        {
          (() => {
            if (this.props.hasOwnProperty('header')) {
              return (
                <div className='message-header'>
                  {this.props.header}
                </div>
              )
            }
          })()
        }
        <div className='message-body'>
          {this.props.children}
        </div>
      </article>
    )
  }
}
