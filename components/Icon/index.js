import React, {Component} from 'react'

export default class BulmaIcon extends Component {
  render () {
    if (this.props.hasOwnProperty('partner')) {
      return (
        <span className={`icon ${this.props.className}`}>
          <i className={`fa fa-${this.props.children}`}></i>
        </span>
      )
    } else {
      return (<i className={`fa fa-${this.props.children} ${this.props.className}`}></i>)
    }
  }
}

BulmaIcon.defaultProps = {
  className: ''
}
