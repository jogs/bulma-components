import React, {Component} from 'react'
import Icon from '../Icon'

export default class BulmaTextField extends Component {
  constructor (p) {
    super(p)
    this.state = {
      value: '',
      valid: null
    }
    this.handleChange = this.handleChange.bind(this)
    this.validate = this.validate.bind(this)
  }
  handleChange (event) {
    let value = event.target.value
    this.setState({value, valid: this.validate(value)}, () => {
      if (this.props.callback) {
        this.props.callback(this.state)
      }
    })
  }
  validate (value) {
    if (this.props.required) {
      return (value !== '' && this.props.pattern.test(value))
    } else return this.props.pattern.test(value)
  }
  _useIcon () {
    if (this.props.hasOwnProperty('icon')) {
      return 'has-icon'
    } else {
      return (this.props.iconRight) ? 'has-icon has-icon-right' : ''
    }
  }
  _classState () {
    return (this.state.valid === null) ? ((this.props.required) ? (
      'is-warning') : '') : ((this.state.valid) ? 'is-success' : 'is-danger')
  }
  render () {
    return (
      <p className={`control ${this._useIcon()} ${this.props.control}`}>
        {(this.props.area) ? (
          <textarea
            className={`textarea ${this._classState()}`}
            type={this.props.type}
            placeholder={this.props.placeholder}
            onChange={this.handleChange}
            value={this.state.value}
          ></textarea>
        ) : (
          <input
            className={`input ${this._classState()}`}
            type={this.props.type}
            placeholder={this.props.placeholder}
            onChange={this.handleChange}
          />
        )}
        {
          (() => {
            if (this.props.iconRight && (typeof this.state.valid === 'boolean')) {
              if (this.state.valid) {
                return <Icon>check</Icon>
              } else {
                return <Icon>warning</Icon>
              }
            } else if (this.props.hasOwnProperty('icon')) {
              return <Icon>{this.props.icon}</Icon>
            }
          })()
        }
        {
          (() => {
            switch (this._classState()) {
              case 'is-warning':
                return (
                  <span className='help is-warning'>
                    {this.props.warning}
                  </span>
                )
              case 'is-danger':
                return (
                  <span className='help is-danger'>
                    {this.props.error}
                  </span>
                )
              case 'is-success':
                return (
                  <span className='help is-success'>
                    {this.props.success}
                  </span>
                )
              default:
                return (<span className='help'>.</span>)
            }
          })()
        }
      </p>
    )
  }
}

BulmaTextField.defaultProps = {
  type: 'text',
  placeholder: '',
  iconRight: false,
  required: false,
  success: 'OK',
  error: 'Error!',
  warning: 'Warning!',
  pattern: new RegExp(),
  area: false,
  control: ''
}
