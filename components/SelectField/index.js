import React, {Component} from 'react'
import uuid from 'uuid'

export default class BulmaSelectField extends Component {
  constructor (p) {
    super(p)
    this.state = {
      value: this.props.value
    }
    this.parseOption = this.parseOption.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange (event) {
    this.setState({value: event.target.value}, () => {
      if (this.props.callback) {
        this.props.callback(this.state)
      }
    })
  }
  parseOption (op) {
    if (typeof op === 'object') {
      if (op.hasOwnProperty('label') && op.hasOwnProperty('value')) {
        return <option
          key={uuid.v4()}
          value={op.value}>
          {op.label}
        </option>
      } else if (op.hasOwnProperty('value')) {
        return <option
          key={uuid.v4()}
          value={op.value}>
          {op.value}
        </option>
      }
    } else {
      return <option
        key={uuid.v4()}
        value={op}>
        {op}
      </option>
    }
  }
  render () {
    return (
      <p className='control'>
        <span className='select'>
          <select onChange={this.handleChange} value={this.state.value}>
            {this.props.options.map(this.parseOption)}
          </select>
        </span>
      </p>
    )
  }
}

BulmaSelectField.defaultProps = {
  options: [],
  value: null
}
