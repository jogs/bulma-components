import React, {Component} from 'react'
import Flatpickr from 'react-flatpickr'
import Icon from '../Icon'

export default class BulmaDatePicker extends Component {
  constructor (p) {
    super(p)
    this.state = {
      value: new Date()
    }
    this.handleChange = this.handleChange.bind(this)
    this.options = this.options.bind(this)
  }
  handleChange (array) {
    this.setState({value: array[0]}, () => {
      if (this.props.callback) {
        this.props.callback(this.state)
      }
    })
  }
  options () {
    let options = {}
    if (this.props.hasOwnProperty('min')) {
      options['minDate'] = this.props.min
    }
    if (this.props.hasOwnProperty('max')) {
      options['maxDate'] = this.props.max
    }
    if (this.props.hasOwnProperty('locale')) {
      options['locale'] = this.props.locale
    }
    return options
  }
  render () {
    return (
      <p className='control has-icon'>
        <Flatpickr
          options={this.options()}
          className='input'
          value={this.state.value}
          onChange={this.handleChange}
        />
        <Icon>calendar</Icon>
      </p>
    )
  }
}
