import React, {Component} from 'react'
import uuid from 'uuid'

export default class BulmaCheckList extends Component {
  constructor (p) {
    super(p)
    this.state = {
      values: new Set(this.props.values)
    }
    this.handleChange = this.handleChange.bind(this)
    this.parseOption = this.parseOption.bind(this)
  }
  handleChange (event) {
    let value = this.props.type(event.target.value)
    let values = new Set([...this.state.values])
    if (values.has(value)) {
      values.delete(value)
    } else {
      values.add(value)
    }
    this.setState({values}, () => {
      if (this.props.callback) {
        this.props.callback(this.state)
      }
    })
  }
  parseOption (op) {
    if (typeof op === 'object') {
      if (op.hasOwnProperty('label') && op.hasOwnProperty('value')) {
        return <span
          key={uuid.v4()}
          className='column'
        >
          <label
            className='checkbox'
          >
            <input
              type='checkbox'
              name={this.props.name}
              value={op.value}
              checked={this.state.values.has(op.value)}
              onChange={this.handleChange}
            />
            {op.label}
          </label>
        </span>
      } else if (op.hasOwnProperty('value')) {
        return <span
          key={uuid.v4()}
          className='column'
        >
          <label
            className='checkbox'
          >
            <input
              type='checkbox'
              name={this.props.name}
              value={op.value}
              checked={this.state.values.has(op.value)}
              onChange={this.handleChange}
            />
            {op.value}
          </label>
        </span>
      }
    } else {
      return <span
        key={uuid.v4()}
        className='column'
      >
        <label
          className='checkbox'
        >
          <input
            type='checkbox'
            name={this.props.name}
            value={op}
            checked={this.state.values.has(op)}
            onChange={this.handleChange}
          />
          {op}
        </label>
      </span>
    }
  }
  render () {
    return (
      <p className='control columns is-gapless'>
        {this.props.options.map(this.parseOption)}
      </p>
    )
  }
}

BulmaCheckList.defaultProps = {
  values: [],
  options: [],
  type: String
}
