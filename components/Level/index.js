import React, {Component} from 'react'

class BulmaLevel extends Component {
  render () {
    return (
      <nav className={`level ${this.props.className}`}>
        {this.props.children}
      </nav>
    )
  }
}

BulmaLevel.defaultProps = {
  className: ''
}

class BulmaLevelItem extends Component {
  render () {
    return (
      <div className={`level-${this.props.type} ${this.props.className}`}>
        {this.props.children}
      </div>
    )
  }
}

BulmaLevelItem.defaultProps = {
  type: 'item',
  className: ''
}

class BulmaLevelResume extends Component {
  render () {
    return (
      <div className={`level-item has-text-centered ${this.props.className}`}>
        <p className='heading'>{this.props.heading}</p>
        <p className='title'>{this.props.title}</p>
      </div>
    )
  }
}

BulmaLevelResume.defaultProps = {
  className: ''
}

const LevelItem = BulmaLevelItem
const LevelResume = BulmaLevelResume

export {
  BulmaLevel as default,
  LevelItem,
  LevelResume
}
