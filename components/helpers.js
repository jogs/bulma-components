const device = /(?:(?:mobile|tablet|touch|desktop|widescreen)(?:-only)?)/.source
const is = /(?:is-)?/.source
const size = /([\w/]*(?:-[\w])?)/.source
const SIZE = new RegExp(`^${is}${size}(-${device})?$`)
const MULTISIZE = new RegExp(`^${SIZE.source}(?:\\s${SIZE.source})+$`)
const DEVICE = new RegExp(`^${is}(${device})$`)

const _parseSize = (string, prefix) => {
  let dev = string.match(SIZE)
  if (dev.length > 1) {
    let size = dev[1] || ''
    let option = dev[2] || ''
    if (size === 'three-quarters' || size === '3/4') {
      return `${prefix}-three-quarters${option}`
    } else if (size === 'two-thirds' || size === '2/3') {
      return `${prefix}-two-thirds${option}`
    } else if (size === 'half' || size === '1/2') {
      return `${prefix}-half${option}`
    } else if (size === 'one-third' || size === '1/3') {
      return `${prefix}-one-third${option}`
    } else if (size === 'one-quarter' || size === '1/4') {
      return `${prefix}-one-quarter${option}`
    } else if (size === 'one' || size === '1') {
      return `${prefix}-one${option}`
    } else if (size === 'two' || size === '2') {
      return `${prefix}-two${option}`
    } else if (size === 'three' || size === '3') {
      return `${prefix}-three${option}`
    } else if (size === 'four' || size === '4') {
      return `${prefix}-four${option}`
    } else if (size === 'five' || size === '5') {
      return `${prefix}-five${option}`
    } else if (size === 'six' || size === '6') {
      return `${prefix}-six${option}`
    } else if (size === 'seven' || size === '7') {
      return `${prefix}-seven${option}`
    } else if (size === 'eight' || size === '8') {
      return `${prefix}-eight${option}`
    } else if (size === 'nine' || size === '9') {
      return `${prefix}-nine${option}`
    } else if (size === 'ten' || size === '10') {
      return `${prefix}-ten${option}`
    } else if (size === 'eleven' || size === '11') {
      return `${prefix}-eleven${option}`
    } else return ''
  } else return ''
}

const parseSize = (string, prefix = 'is') => {
  if (MULTISIZE.test(string)) {
    return string.split(' ')
    .map((str) => _parseSize(str, prefix))
    .join(' ')
  } else {
    return _parseSize(string, prefix)
  }
}

export {
  parseSize,
  SIZE,
  MULTISIZE,
  DEVICE
}
