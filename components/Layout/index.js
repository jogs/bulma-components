import React, {Component} from 'react'

class BulmaContainer extends Component {
  render () {
    return (
      <div className={`container ${this.props.className}`}>
        {this.props.children}
      </div>
    )
  }
}

class BulmaHero extends Component {
  injectHeader () {
    if (this.props.hasOwnProperty('header')) {
      return (
        <div className='hero-head'>
          {this.props.header}
        </div>
      )
    }
  }
  injectFooter () {
    if (this.props.hasOwnProperty('footer')) {
      return (
        <div className='hero-foot'>
          {this.props.footer}
        </div>
      )
    }
  }
  render () {
    return (
      <section className={`hero ${this.props.className} ${this.props.size}`}>
        {this.injectHeader()}
        <div className='hero-body'>
          {this.props.children}
        </div>
        {this.injectFooter()}
      </section>
    )
  }
}

class BulmaFooter extends Component {
  render () {
    return (
      <footer className='footer'>
        {this.props.children}
      </footer>
    )
  }
}

const Hero = BulmaHero
const Footer = BulmaFooter

export {
  BulmaContainer as default,
  Hero,
  Footer
}
