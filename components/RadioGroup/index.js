import React, {Component} from 'react'
import uuid from 'uuid'

export default class BulmaRadioGroup extends Component {
  constructor (p) {
    super(p)
    this.state = {
      value: this.props.value
    }
    this.handleChange = this.handleChange.bind(this)
    this.parseOption = this.parseOption.bind(this)
  }
  handleChange (event) {
    this.setState({value: this.props.type(event.target.value)}, () => {
      if (this.props.callback) {
        this.props.callback(this.state)
      }
    })
  }
  parseOption (op) {
    if (typeof op === 'object') {
      if (op.hasOwnProperty('label') && op.hasOwnProperty('value')) {
        return <label
          key={uuid.v4()}
          className='radio'
        >
          <input
            type='radio'
            name={this.props.name}
            value={op.value}
            checked={this.state.value === op.value}
            onChange={this.handleChange}
          />
          {op.label}
        </label>
      } else if (op.hasOwnProperty('value')) {
        return <label
          key={uuid.v4()}
          className='radio'
        >
          <input
            type='radio'
            name={this.props.name}
            value={op.value}
            checked={this.state.value === op.value}
            onChange={this.handleChange}
          />
          {op.value}
        </label>
      }
    } else {
      return <label
        key={uuid.v4()}
        className='radio'
      >
        <input
          type='radio'
          name={this.props.name}
          value={op}
          checked={this.state.value === op}
          onChange={this.handleChange}
        />
        {op}
      </label>
    }
  }
  render () {
    return (
      <p className='control'>
        {this.props.options.map(this.parseOption)}
      </p>
    )
  }
}

BulmaRadioGroup.defaultProps = {
  value: null,
  option: [],
  type: String
}
