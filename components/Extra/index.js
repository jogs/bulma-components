import React, {Component} from 'react'

class BulmaControl extends Component {
  render () {
    return (
      <p className={`control ${this.props.className}`}>
        {this.props.children}
      </p>
    )
  }
}

BulmaControl.defaultProps = {
  className: ''
}

class BulmaAddon extends Component {
  render () {
    return (
      <p className={`control has-addons ${this.props.className} ${(this.props.align) ? `has-addons-${this.props.align}` : ''}`}>
        {this.props.children}
      </p>
    )
  }
}

BulmaAddon.defaultProps = {
  align: undefined,
  className: ''
}

const Control = BulmaControl
const Addon = BulmaAddon

export {
  Addon as default,
  Control
}
