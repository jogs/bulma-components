import React, {Component} from 'react'
import {parseSize} from '../helpers'

class BulmaColumns extends Component {
  render () {
    return (
      <div className={`columns ${(this.props.multiline) ? 'is-multiline' : ''} ${(this.props.gapless) ? 'is-gapless' : ''} ${(this.props.device) ? `is-${this.proos.device}` : ''} ${this.props.className}`}>
        {this.props.children}
      </div>
    )
  }
}

BulmaColumns.defaultProps = {
  multiline: false,
  gapless: false,
  device: undefined,
  className: ''
}

const Columns = BulmaColumns

class BulmaColumn extends Component {
  render () {
    return (
      <div
        className={
          `column ${parseSize(this.props.size)} ${parseSize(this.props.offset, 'is-offset')} ${this.props.className}`
        }
      >
        {this.props.children}
      </div>
    )
  }
}

BulmaColumn.defaultProps = {
  size: '',
  offset: '',
  className: ''
}

const Column = BulmaColumn

class BulmaTile extends Component {
  render () {
    return (
      <div
        className={
          `tile ${(this.props.vertical) ? 'is-vertical' : ''} ${(this.props.ancestor) ? 'is-ancestor' : ''} ${(this.props.parent) ? 'is-parent' : ''} ${(this.props.child) ? 'is-child' : ''} ${(this.props.size) ? (`is-${this.props.size}`) : ''} ${this.props.className}`
        }
      >
        {this.props.children}
      </div>
    )
  }
}

BulmaTile.defaultProps = {
  className: '',
  vertical: false,
  ancestor: false,
  parent: false,
  child: false,
  size: undefined
}

const Tile = BulmaTile

export {
  Columns,
  Column,
  Tile
}
