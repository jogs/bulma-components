import React, {Component} from 'react'

const icon = (iconName) => (<span className='icon'>
  <i className={`fa fa-${iconName}`}></i>
</span>)

export default class BulmaButton extends Component {
  render () {
    return (
      <button
        type={this.props.type}
        className={`button ${this.props.className}`}
        >
        {
          (() => {
            if (!this.props.iconRight && this.props.hasOwnProperty('icon')) {
              return icon(this.props.icon)
            }
          })()
        }
        <span>{this.props.children}</span>
        {
          (() => {
            if (this.props.iconRight && this.props.hasOwnProperty('icon')) {
              return icon(this.props.icon)
            }
          })()
        }
      </button>
    )
  }
}

BulmaButton.defaultProps = {
  iconRight: false,
  type: 'button'
}
