import React, {Component} from 'react'
import uuid from 'uuid'
class BulmaTabs extends Component {
  constructor (p) {
    super(p)
    this.state = {
      active: this.props.active || this.props.elements[0] || null
    }
    this.parseTabs = this.parseTabs.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange (index) {
    if (this.state.active !== this.props.elements[index]) {
      this.setState({active: this.props.elements[index]}, () => {
        if (this.props.callback) {
          this.props.callback(this.state)
        }
      })
    }
  }
  parseTabs (tab, index) {
    if (typeof tab === 'object') {
      if (tab.hasOwnProperty('label') && tab.hasOwnProperty('content')) {
        return (
          <li key={uuid.v4()} className={
            (this.state.active === tab.label) ? 'is-active' : ''
          } onClick={() => this.handleChange(index)}>
            {tab.content}
          </li>
        )
      }
    } else {
      return (
        <li key={uuid.v4()} className={
          (this.state.active === tab) ? 'is-active' : ''
        } onClick={() => this.handleChange(index)}>
          <a>{tab}</a>
        </li>
      )
    }
  }
  injectTabs () {
    if (this.props.hasOwnProperty('container')) {
      return (<div className='container'>
        <ul className={`is-${this.props.position}`}>
          {this.props.elements.map(this.parseTabs)}
        </ul>
      </div>)
    } else {
      return (
        <ul className={`is-${this.props.position}`}>
          {this.props.elements.map(this.parseTabs)}
        </ul>
      )
    }
  }
  render () {
    return (
      <nav className={`tabs ${this.props.className}`}>
        {this.injectTabs()}
      </nav>
    )
  }
}

BulmaTabs.defaultProps = {
  elements: [],
  className: '',
  position: 'left'
}

export default BulmaTabs
