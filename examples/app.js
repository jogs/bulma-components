import 'flatpickr/dist/themes/material_green.css'
import 'bulma/css/bulma.css'
import {es} from 'flatpickr/dist/l10n/es.js'
import React, {Component} from 'react'
import {render} from 'react-dom'

import {
  Addon,
  Button,
  CheckList,
  Column,
  Columns,
  Container,
  DatePicker,
  Footer,
  Hero,
  Icon,
  Level,
  LevelItem,
  LevelResume,
  Message,
  RadioGroup,
  SelectField,
  Tabs,
  TextField,
  Tile
} from '../components'

class Header extends Component {
  constructor (p) {
    super(p)
    this.state = {
      tab: 'Example 1'
    }
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange (state) {
    this.setState({tab: state.active})
  }
  render () {
    return (
      <Hero
        className='is-primary'
        footer={
          <Tabs
            elements={['Example 1', 'Example 2', 'Example 3']}
            className='is-medium'
            container
            callback={this.handleChange}
          />
        }
      >
        <Container>
          <h1 className='title'>
            title
          </h1>
          <h2 className='subtitle'>
            subtitle
          </h2>
        </Container>
      </Hero>
    )
  }
}

render(
  (
    <div>
      <Header />
      <br />
      <Columns gapless>
        <Column size='1/4'>
          <Button
            icon='download'
            className='is-primary'
          >bulma-components</Button>
        </Column>
        <Column size='3/4'>
          <DatePicker locale={es} min={new Date()} />
        </Column>
      </Columns>
      <Tabs
        elements={[1, 2, 3, 4, 5]}
        active={5}
        className='is-boxed'
        position='right'
      />
      <Tabs
        elements={[
          {
            label: 'Images',
            content: (<a>
              <Icon partner>image</Icon>
              <span>Pictures</span>
            </a>)
          },
          {
            label: 'Music',
            content: (<a>
              <Icon partner>music</Icon>
              <span>Music</span>
            </a>)
          },
          {
            label: 'Video',
            content: (<a>
              <Icon partner>film</Icon>
              <span>Video</span>
            </a>)
          }
        ]}
      />
      <Level>
        <LevelResume heading='example' title='1' />
        <LevelResume heading='example' title='2' />
        <LevelResume heading='example' title='3' />
        <LevelResume heading='example' title='4' />
      </Level>
      <br />
      <Level>
        <LevelItem type='left'>
          <TextField
            placeholder='Example'
            required
            pattern={/^[\w]{8}$/}
            error=':('
            warning='is required'
            iconRight
          />
        </LevelItem>
        <LevelItem type='right'>
          <Addon>
            <TextField icon='lock' placeholder='Password' type='password' />
            <Button className='is-danger'>password</Button>
          </Addon>
        </LevelItem>
      </Level>
      <br />
      <Container>
        <Tile ancestor>
          <Tile vertical size='8'>
            <Tile>
              <Tile parent vertical>
                <Tile child className='box'>...</Tile>
                <Tile child className='box'>...</Tile>
              </Tile>
              <Tile parent>
                <Tile child className='box'>...</Tile>
              </Tile>
            </Tile>
            <Tile parent>
              <Tile child className='box'>...</Tile>
            </Tile>
          </Tile>
          <Tile parent>
            <Tile child className='box'>...</Tile>
          </Tile>
        </Tile>
      </Container>
    </div>
  ),
document.getElementById('app'))
